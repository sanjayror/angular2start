var http = require('http');
var fs = require('fs');
var express = require('express');
var app = express();

app.get('/', function (req, res) {
    res.send('Json Server Successfully Running..');
})

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/Employees', function (req, res) {
    res.sendFile(__dirname + "/db.json" );
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
      
   console.log("Json Server : app listening at :%s", port)
})
