// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
      apiKey: 'AIzaSyCfsQgFXmS3dm_FRhLNgCvvtnGXFywnm_Y',
      authDomain: 'fb-host-test-2336c.firebaseapp.com',
      databaseURL: 'https://fb-host-test-2336c.firebaseio.com/',
      projectId: 'ffb-host-test-2336c',
      storageBucket: 'fcc-book-trading-173021.appspot.com',
      messagingSenderId: '763399536402'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
