import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

import { DialogOverviewExampleDialog } from './ngmaterial/DialogOverviewExampleDialog';
import { systemComponents, systemModules, myComponents, myServices, AngularMaterial, myPipes, myDirectives } from './app.common';
import { CpalertComponent } from './cpalert/cpalert.component';
import { FirebaseComponent } from './firebase/firebase.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    myComponents,
    myPipes,
    myDirectives,
    systemComponents,
    CpalertComponent,
    DialogOverviewExampleDialog,
    AppComponent,
    FirebaseComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AngularMaterial,
    systemModules,
    AngularFireModule.initializeApp(environment.firebase, 'fb-host-test'),
    AngularFireDatabaseModule
  ],
  exports: [
    AngularMaterial
  ],
  providers: [ myServices ],
  bootstrap: [AppComponent],
  entryComponents: [DialogOverviewExampleDialog]
})
export class AppModule { }
