import { Component, OnInit } from '@angular/core';
import { MydataserviceService } from '../Service/mydataservice.service';
import { Photos, PhotosObj } from '../_modal';

@Component({
  selector: 'app-scroller',
  templateUrl: './scroller.component.html',
  styleUrls: ['./scroller.component.css'],
})
export class ScrollerComponent implements OnInit {

  title = 'app';
  myPhotosList: Photos[] = [];
  page = 1;

  constructor(private service: MydataserviceService) { }

  ngOnInit() {
    // To call api for initial image rendering
    this.getPhotos();
  }

  // To get image data from api
  getPhotos() {
    console.log(this.page);
    this.service.getMyPhotos(this.page).subscribe((res) => this.onSuccess(res));
  }

  // When we got data on a success
  onSuccess(res) {
    console.log(res);
    if (res !== undefined) {
      res.forEach(item => {
        this.myPhotosList.push(new PhotosObj(item));
      });
    }
  }

  // When scroll down the screen
  onScroll() {
    console.log('Scrolled');
    this.page = this.page + 1;
    this.getPhotos();
  }

}
