import { Directive, ElementRef, Input, OnInit, OnChanges, SimpleChanges, DoCheck, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appDirCustom]'
})
export class DirCustomDirective implements OnInit, OnChanges, DoCheck, OnDestroy {
  @Input() politicianAuthor: string;
  @Input() politicianQuote: string;

  constructor(private el: ElementRef) {
    console.log('Constructor called..............');
  }

  ngOnChanges(changes: SimpleChanges) {
    this.el.nativeElement.innerHTML = `${this.politicianAuthor} &nbsp; ${this.politicianQuote} &nbsp;`;
    console.log(`ngOnChanges called (${changes.politicianAuthor.currentValue} , ${changes.politicianAuthor.previousValue})..............`);
  }

  ngOnInit() {
    console.log('ngOnInit called..............');

    this.el.nativeElement.style.backgroundColor = 'yellow';
    this.el.nativeElement.innerHTML = `${this.politicianAuthor} &nbsp; ${this.politicianQuote} &nbsp;`;

    // console.log('input-box keys  : ', this.politicianAuthor, this.politicianQuote);
  }

  ngDoCheck() {
    console.log('ngDoCheck called..............');
  }

  ngOnDestroy() {
    console.log('ngOnDestroy called..............');
  }
}
