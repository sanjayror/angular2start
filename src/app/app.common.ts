import { AggridComponent } from './aggrid/aggrid.component';
import { AgGridModule } from 'ag-grid-angular';
import { Ng2TableComponent } from './ng2-table/ng2-table.component';
export const systemComponents = [
    AggridComponent,
    Ng2TableComponent,
];

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ng2-bootstrap/pagination';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
export const systemModules = [
    AgGridModule.withComponents([]),
    Ng2TableModule,
    PaginationModule.forRoot(),
    InfiniteScrollModule,
    BrowserAnimationsModule
];

import { EmployeeComponent } from './employee/employee.component';
import { EmployeeListComponent } from './employee/employeeList.component';
import { EmployeeCountComponent } from './employee/employeeCount.component';
import { SimpleComponent } from './others/simple.component';
import { ComboboxComponent } from './combobox/combobox.component';
import { ScrollerComponent } from './scroller/scroller.component';
import { NgmaterialComponent } from './ngmaterial/ngmaterial.component';
import { DialogOverviewExample } from './ngmaterial/dialog-overview-example';
import { CheckboxComponent } from './ngmaterial/checkbox.component';

export const myComponents = [
    EmployeeComponent,
    EmployeeListComponent,
    EmployeeCountComponent,
    SimpleComponent,
    ComboboxComponent,
    ScrollerComponent,
    NgmaterialComponent,
    DialogOverviewExample,
    CheckboxComponent,
];

import {
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
} from '@angular/material';

export const AngularMaterial = [
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
];

import { EmployeeService } from './employee/employee.service';
import { MydataserviceService } from './Service/mydataservice.service';
export const myServices = [
    EmployeeService,
    MydataserviceService
];

import { DirCustomDirective } from './dir-custom.directive';
export const myDirectives = [
    DirCustomDirective
];

import { EmployeeTitlePipe } from './employee/employeeTitle.pipe';
import { FilterPipePipe } from './filter-pipe.pipe';
export const myPipes = [
    EmployeeTitlePipe,
    FilterPipePipe
];
