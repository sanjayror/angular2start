import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './app.component.html',
  // template : `<h1>
  //                Hello {{name}}  <br />
  //                {{ Concat }}
  //             </h1>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Angular2Start';
  name = 'Angular';

  Concat = `Title - ${this.title}`;
  pageHeader = 'Employee Details';
  imagePath = 'http://www.pragimtech.com/Images/logo.JPG';
  imagePath2 = 'Images/logo.JPG';
  isDisabled = false;
  badHtml = 'hello <script> alert("hello"); </script> world';

  firstName = 'Tom';
  lastName = 'Hopkins';
  classesToApply = 'italicsClass boldClass';
  applyBoldClass = true;
  applyItalicsClass = true;
  isBold = true;
  fontSize = 30;
  isItalic = true;
  names = 'Tom';
  userText = 'Pragim';
  SelectedInput: string;
  hero = true;

  friends = [
    { name: 'Lucky', selected: false },
    { name: 'Yoga', selected: false },
    { name: 'Fauzan', selected: false },
    { name: 'Cecep', selected: true },
    { name: 'Nurjaman', selected: false },
    { name: 'Asti', selected: false },
  ];

  ngOnInit() {
    for (let i = 0; i <= 100; i++) {
      this.friends.push({ name: 'name' + i, selected: false });
    }
  }

  getFullName(): string {
    return this.firstName + ' ' + this.lastName;
  }

  addClasses() {
    const classes = {
      boldClass: this.applyBoldClass,
      italicsClass: this.applyItalicsClass
    };

    return classes;
  }

  addStyles() {
    const styles = {
      'font-size.px': this.fontSize,
      'font-style': this.isItalic,
      'font-weight': this.isBold ? 'bold' : 'normal',
    };

    return styles;
  }

  onClick(): void {
    console.log('Button Clicked');
  }
}
