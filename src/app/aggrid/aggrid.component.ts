import { Component, OnInit } from '@angular/core';
// import { fullFormService } from '../service/fullForm.service';
import { AgGridModule } from 'ag-grid-angular';
import { Grid, GridOptions } from 'ag-grid-community';

@Component({
  selector: 'app-aggrid',
  templateUrl: './aggrid.component.html',
  styleUrls: ['./aggrid.component.css']
})
export class AggridComponent implements OnInit {

  gridOptions: GridOptions;

  fullForms = 'Hi Full Form';
  fullFormDataGrid: any = [];

  SelectedName = '';
  SelectedAddress = '';
  SelectedSalary = 0;

  SelectedDepartment: number;
  SelectedItemCategory: number;
  SelectedItemType: number;

  DepartmentData: any = [];
  ItemCategoryData: any = [];
  ItemTypeData: any = [];
  errorMessage: string;

  // constructor(private _fullFormService: fullFormService) {
  //   this.SelectedDepartment = 0;
  // }

  cellRendererGridConfigComboBox = (params) => {
    const eSelect = document.createElement('select');

    setTimeout(() => {

      const rowIndex = params.rowIndex;
      const Column = params.eGridCell.attributes.colId;

      const setSpoolNoList: any = [];
      const setLineISOMetricNoList: any = [];

      const editing = false;
      eSelect.setAttribute('class', 'custom-select form-control');
      eSelect.setAttribute('name', params.colDef.field);
      eSelect.setAttribute('id', params.colDef.field + '_' + rowIndex);

      const value = params.value;

      const eOptions = document.createElement('option');
      eOptions.text = 'Select';
      eOptions.value = 'Select';
      eSelect.appendChild(eOptions);

      for (let iRows = 0; iRows < this.DepartmentData.length; iRows++) {
        const eOptionNg = document.createElement('option');
        eOptionNg.text = this.DepartmentData[iRows].Name;
        eOptionNg.value = this.DepartmentData[iRows].DepartmentID;
        eSelect.appendChild(eOptionNg);
      }

    }, 1000);

    return eSelect;
  }

  cellRendererGridConfigTextBox(params) {
    const editing = false;
    const eTextbox = document.createElement('input');
    eTextbox.setAttribute('type', 'text');
    eTextbox.setAttribute('class', 'form-control');

    const Column = params.eGridCell.attributes.colId;
    if (params.data[Column] !== undefined) {
      eTextbox.setAttribute('value', params.data[Column]);
    }

    if (Column === 'Salary') {
      eTextbox.setAttribute('readOnly', 'true');
    } else {
      const value = params.value;
    }

    return eTextbox;
  }

  // call for anything on page load.
  ngOnInit() {
    // const response = this._fullFormService.someMethod();
    this.fnGetDepartments();
    this.fnGetItemCategory();
    this.fnAggridBind();
  }

  fnAggridBind(): void {
    // AggridBind Here
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = [];
    this.gridOptions.columnDefs.push({ headerName: 'Department', field: 'Department', cellRenderer: this.cellRendererGridConfigComboBox },
      { headerName: 'Item Category', field: 'ItemCategory', cellRenderer: this.cellRendererGridConfigTextBox },
      { headerName: 'Item Type', field: 'ItemType', cellRenderer: this.cellRendererGridConfigTextBox },
      { headerName: 'Name', field: 'Name', cellRenderer: this.cellRendererGridConfigTextBox },
      { headerName: 'Salary', field: 'Salary', cellRenderer: this.cellRendererGridConfigTextBox },
      { headerName: 'Address', field: 'Address', cellRenderer: this.cellRendererGridConfigTextBox },
    );

    // const el = document.querySelector('#grid-test');
    // // tslint:disable-next-line:no-unused-expression
    // new Grid(<HTMLDivElement>el, this.gridOptions);

    const data: any[] = [];
    data.push({ Department: 'Name 1', ItemCategory: 'Category A', ItemType: 'ItemType', Name: 'Name', Salary: 0, Address: 'address' });
    data.push({ Department: 'Name 1', ItemCategory: 'Category A', ItemType: 'ItemType', Name: 'Name', Salary: 0, Address: 'address' });
    data.push({ Department: 'Name 1', ItemCategory: 'Category A', ItemType: 'ItemType', Name: 'Name', Salary: 0, Address: 'address' });
    data.push({ Department: 'Name 1', ItemCategory: 'Category A', ItemType: 'ItemType', Name: 'Name', Salary: 0, Address: 'address' });
    data.push({ Department: 'Name 1', ItemCategory: 'Category A', ItemType: 'ItemType', Name: 'Name', Salary: 0, Address: 'address' });
    data.push({ Department: 'Name 1', ItemCategory: 'Category A', ItemType: 'ItemType', Name: 'Name', Salary: 0, Address: 'address' });
    this.gridOptions.rowData = data;
    // this.gridOptions.api.setRowData(data);
    // AggridBind Here

    this.gridOptions.context = {
      myService: this.DepartmentData
    };

    this.gridOptions.rowData = [
      { id: 5, value: 10 },
      { id: 10, value: 15 },
      { id: 15, value: 20 }
    ];
  }

  fnGetDepartments(): void {
    this.DepartmentData = [
      { 'DepartmentID': 1, Name: 'IT' },
      { 'DepartmentID': 2, Name: 'Sales' },
      { 'DepartmentID': 3, Name: 'HR' },
      { 'DepartmentID': 4, Name: 'Legal' }];
  }

  fnGetItemCategory(): void {
    // this._fullFormService.fnGetItemCategory().subscribe(
    //   data => {
    //     this.ItemCategoryData = JSON.parse(data['_body']);
    //   }
    // );
  }

  fnGetItemType(): void {
    const json = { ItemCategoryId: this.SelectedItemCategory };
    this.SelectedItemType = 0;

    // this._fullFormService.fnGetItemType(json).subscribe(
    //   data => {
    //     this.ItemTypeData = JSON.parse(data['_body']);
    //   }
    // );
  }

  fnReset(): void {
    this.SelectedName = '';
    this.SelectedAddress = '';
    this.SelectedSalary = 0;

    this.SelectedDepartment = 0;
    this.SelectedItemCategory = 0;
    this.SelectedItemType = 0;
  }

  // TODO: Remove this when we're done
  get diagnostic() {
    return JSON.stringify(this.fullFormDataGrid);
  }


}
