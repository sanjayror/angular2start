import { Injectable } from '@angular/core';
import { IEmployee } from './employee';
import { Observable, of, throwError, ErrorObserver, observable } from 'rxjs';
import { map, mapTo, delay, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class EmployeeService {
    url = 'http://localhost:8081';

    constructor(private _httpClient: HttpClient) {

    }

    private handleError(errorResponse: HttpErrorResponse) {
        if (errorResponse.error instanceof ErrorEvent) {
            console.error('Client Side Error : ', errorResponse.error.message);
        } else {
            console.error('Server Side Error : ', errorResponse);
        }

        return Observable.throw('There is problem with the service, We are notified and working on it !!');
    }

    getEmployees(): Observable<IEmployee[]> {
        // return this._httpClient.get((`${this.url}/Employees`).pipe(map((val: IEmployee[]) => val));
        // return this._httpClient.get<IEmployee[]>((`${this.url}/Employees`);

        return this._httpClient.get(`${this.url}/Employees`).pipe(map((val: IEmployee[]) => val), catchError(this.handleError), delay(999));
    }
}
