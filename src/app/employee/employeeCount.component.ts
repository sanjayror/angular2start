import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-employee-count',
    templateUrl: './employeeCount.component.html',
    styleUrls: ['./employeeCount.component.css'],
})
export class EmployeeCountComponent {
    selectedRadioButtonValue = 'All';

    @Output()
    radioButtonSelectionChanged: EventEmitter<string> = new EventEmitter<string>();

    @Input()
    all = 10;

    @Input()
    male = 5;

    @Input()
    female = 5;

    onRadioButtonSeletionChange() {
      this.radioButtonSelectionChanged.emit(this.selectedRadioButtonValue);
      console.log(this.selectedRadioButtonValue);
    }
}
