import { Component, OnInit } from '@angular/core';
import { IEmployee } from './employee';
import { EmployeeService } from './employee.service';

@Component({
    selector: 'app-list-employees',
    templateUrl: './employeeList.component.html',
    styleUrls: ['./employeeList.component.css'],
    providers: [EmployeeService]
})
export class EmployeeListComponent implements OnInit {
    employees: IEmployee[];
    AllEmployees: any[];
    selectedEmployeeCountRadioButton = 'All';

    constructor(private _employeeService: EmployeeService) {

    }

    ngOnInit() {
        // is lifecycle hook and Run after constructor.
        this._employeeService.getEmployees().subscribe((data) =>
            this.employees = data,
            ((error) => console.log(`error : ${error}`))
        );
    }

    getEmployees(): void {
        this._employeeService.getEmployees().subscribe((data) =>
            this.employees = data // ['employees']
        );
    }

    trackByEmpCode(index: number, employee: any): string {
        return employee.code;
    }

    getAllCount(): number {
        return this.employees.length;
    }

    getmaleCount(): number {
        return this.employees.filter(t => t.gender === 'Male').length;
    }

    getfemaleCount(): number {
        return this.employees.filter(t => t.gender === 'Female').length;
    }

    onEmployeeCountRadioButtonChange(SelectedRadioButtonValue: any): void {
        this.selectedEmployeeCountRadioButton = SelectedRadioButtonValue;
        console.log('Admin : ' + SelectedRadioButtonValue);
    }
}
