import { Component, OnInit, AfterViewChecked, AfterViewInit, Input } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-combobox',
  templateUrl: './combobox.component.html',
  styleUrls: ['./combobox.component.css']
})
export class ComboboxComponent implements OnInit, AfterViewChecked, AfterViewInit {
  @Input() CustomName: string;
  @Input() data: any;

  constructor() { }

  selectedAll: false;
  selectedfilter: string;
  friends = this.data;

  friends1 = [
    { name: 'Lucky', selected: false },
    { name: 'Yoga', selected: false },
    { name: 'Fauzan', selected: false },
    { name: 'Cecep', selected: true },
    { name: 'Nurjaman', selected: false },
    { name: 'Asti', selected: false },
  ];

  getSelectedItems = function (item) {
    return item.selected;
  };

  fnSelectAll = function () {
    for (let i = 0; i < this.friends.length; i++) {
      this.friends[i].selected = !this.selectedAll;
    }
  };

  ngOnInit() {
    if (this.friends !== undefined) {
      for (let i = 0; i <= 100; i++) {
        this.friends.push({ name: 'name' + i, selected: false });
      }
    }
  }

  ngAfterViewInit() {
    this.friends = this.data;

    $('#idQuery').click(function () {
      alert('Alert Jquery Clicked');
    });

    $('.selected-items-box').bind('click', function (e) {
      $('.wrapper .list').slideToggle('fast');
    });
  }

  ngAfterViewChecked() {
    // Issue here multiple calling goes here..........
  }
}
