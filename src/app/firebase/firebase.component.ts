import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

export class Book {
  constructor(public title) { }
}

@Component({
  selector: 'app-firebase',
  templateUrl: './firebase.component.html',
  styleUrls: ['./firebase.component.css']
})
export class FirebaseComponent implements OnInit {
  private bookCounter = 0;
  private filter = '';
  public books: AngularFireList<Book[]>;

  constructor(public db: AngularFireDatabase) {
    this.books = db.list('/books');
  }

  public addBook(): void {
    this.books = this.db.list('/books');
    const newBook = new Book(`My book # ${this.bookCounter++}`);
    // this.books.push({ title: 'grokonez.com' });
  }

  public filterBooks(): void {
    this.books = this.db.list('/books');
  }

  ngOnInit() {
  }

}
