import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-cpalert',
  templateUrl: './cpalert.component.html',
  styleUrls: ['./cpalert.component.css']
})
export class CpalertComponent implements OnInit, AfterViewInit {

  constructor() {
    console.log('cpalert - constructor called.........');

    const cars = ['🚗', '🚙', '🚕'];
    for (let index = 0; index < cars.length; index++) {
      console.log(`This is the car ${cars[index]}`);
    }

    $(document).ready(function () {
      console.log('cpalert - (document).ready called.........');
      $('#idDiv').html('hello div 1');
    });

    const val = document.getElementById('idDiv');
  }

  // TODO: Remove this when we're done
  get diagnostic() {
    return JSON.stringify(this.jsonData);
  }

  hello = 'Hello boot';
  SelectedIndex = 0;
  Condition = 0;

  jsonData: any = [
    {
      'SelectedAlertType': '2',
      'SelectedOccurence': '0',
      'SelectedValueSMS': '2222222222',
      'SelectedValueEMail': 'test@aa.com',
      'AlertId': '1',
      'AlertName': 'SOS',
      'AlertType': [
        {
          'key': '0',
          'value': 'SMS'
        },
        {
          'key': '1',
          'value': 'Email'
        },
        {
          'key': '2',
          'value': 'Both',
          'SelectedValue': ''
        }
      ],
      'Occurence': [
        {
          'key': '0',
          'value': 'Always'
        },
        {
          'key': '1',
          'value': 'Once'
        }
      ],
      'Status': '0'
    },
    {
      'SelectedAlertType': '0',
      'SelectedOccurence': '-1',
      'SelectedValueSMS': '3333333333',
      'SelectedValueEMail': '',
      'AlertId': '2',
      'AlertName': 'Geo In',
      'AlertType': [
        {
          'key': '0',
          'value': 'SMS'
        },
        {
          'key': '1',
          'value': 'Email'
        },
        {
          'key': '2',
          'value': 'Both',
          'SelectedValue': ''
        }
      ],
      'Occurence': [
        {
          'key': '0',
          'value': 'Always'
        },
        {
          'key': '1',
          'value': 'Once'
        }
      ],
      'Status': '1'
    },
    {
      'SelectedAlertType': '1',
      'SelectedOccurence': '1',
      'SelectedValueSMS': '',
      'SelectedValueEMail': 'test@cc.com',
      'AlertId': '3',
      'AlertName': 'Geo Out',
      'AlertType': [
        {
          'key': '0',
          'value': 'SMS'
        },
        {
          'key': '1',
          'value': 'Email'
        },
        {
          'key': '2',
          'value': 'Both',
          'SelectedValue': ''
        }
      ],
      'Occurence': [
        {
          'key': '0',
          'value': 'Always'
        },
        {
          'key': '1',
          'value': 'Once'
        }
      ],
      'Status': '1'
    },
    {
      'SelectedAlertType': '-1',
      'SelectedOccurence': '-1',
      'SelectedValueSMS': '',
      'SelectedValueEMail': '',
      'AlertId': '4',
      'AlertName': 'Alert',
      'AlertType': [
        {
          'key': '0',
          'value': 'SMS'
        },
        {
          'key': '1',
          'value': 'Email'
        },
        {
          'key': '2',
          'value': 'Both',
          'SelectedValue': ''
        }
      ],
      'Occurence': [
        {
          'key': '0',
          'value': 'Always'
        },
        {
          'key': '1',
          'value': 'Once'
        }
      ],
      'Status': '1'
    }
  ];

  ngOnInit() {
    console.log('cpalert - ngOnInit called.........');
  }

  ngAfterViewInit() {
    console.log('cpalert - ngAfterViewInit called.........');
    $('#idDiv').html('hello div');
  }

  fnAlertType(id): void {
    this.SelectedIndex = id;
  }

  fnSubmit(): void {
    const myObj = {
      'name': 'John', 'age': 30,
      'cars': [{
        'car1': 'Ford',
        'car2': 'BMW',
        'car3': 'Fiat'
      }]
    };

    const myObj2 = {
      'name': 'John',
      'age': 30,
      'cars': {
        'car1': 'Ford',
        'car2': 'BMW',
        'car3': 'Fiat'
      }
    };

    const someArray = [1, 'string', false];

    for (const entry of someArray) {
      console.log(entry);
    }

    for (let i = 0; i < 5; i++) {

    }

    for (const ids in myObj.cars) {

    }

    const jsonji = {
      'squadName': 'Super hero squad',
      'homeTown': 'Metro City',
      'formed': 2016,
      'secretBase': 'Super tower',
      'active': true,
      'members': [
        {
          'name': 'Molecule Man',
          'age': 29,
          'secretIdentity': 'Dan Jukes',
          'powers': [
            'Radiation resistance',
            'Turning tiny',
            'Radiation blast'
          ]
        },
        {
          'name': 'Madame Uppercut',
          'age': 39,
          'secretIdentity': 'Jane Wilson',
          'powers': [
            'Million tonne punch',
            'Damage resistance',
            'Superhuman reflexes'
          ]
        },
        {
          'name': 'Eternal Flame',
          'age': 1000000,
          'secretIdentity': 'Unknown',
          'powers': [
            'Immortality',
            'Heat Immunity',
            'Inferno',
            'Teleportation',
            'Interdimensional travel'
          ]
        }
      ]
    };

    const json2 = { 'Name': { 'A': 'B', 'C': [{ 'AA': 'B' }] }, 'Name2': {}, 'Name3': { 'NN': {} } };
    const json3 = { 'A': 'B' };
    const json4 = {
      '$schema': 'http://json-schema.org/draft-06/schema#',
      'title': 'Product',
      'description': 'A product from Acmes catalog',
      'type': 'object',
      'properties': { 'id': { 'description': 'product', 'type': 'integer' } },
      'pop': { '1': { '2': { '3': {} } } },
      'required': ['id'],
      'dimensions': {
        'type': 'object',
        'properties': {
          'length': { 'type': 'number' },
          'width': { 'type': 'number' },
          'height': { 'type': 'number' }
        },
        'required': ['length', 'width', 'height']
      },
      'arraya1': ['A', ['length', 'width', 'height']],
      'arraya2': ['A', [{ 'pop': { '1': { '2': { '3': {} } } } }], 2, 'Test'],
      'val1': { 'A': {} },
      'val2': [{ 'A': 'B' }],
      'var3': [{ '1': 'B', 'C': { json4: 'hello' } }]
    };
  }

}
