import { Component, Input, OnChanges, SimpleChanges, OnInit } from '@angular/core';

@Component({
    selector: 'app-simple',
    template: 'Your entered : {{simpleInput}}'
})
export class SimpleComponent implements OnChanges, OnInit {
    @Input() simpleInput: string;

    ngOnInit() {
        console.log('OnInit Life cycle Hook');
    }

    constructor() {
        console.log('Constructor called !!');
    }

    ngOnChanges(changes: SimpleChanges) {
        // tslint:disable-next-line:forin
        for (const propertyName in changes) {
            const change = changes[propertyName];
            const current = JSON.stringify(change.currentValue);
            const previous = JSON.stringify(change.previousValue);
            console.log(`${propertyName} : currentValue = ${current}, previousValue = ${previous}`);
        }
    }
}
