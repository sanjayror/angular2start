import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngmaterial',
  templateUrl: './ngmaterial.component.html',
  styleUrls: ['./ngmaterial.component.css']
})
export class NgmaterialComponent implements OnInit {

  matBadge = 20;
  typesOfShoes: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  sliderValue = 0;
  numbers: any[] = [];

  constructor() { }

  ngOnInit() {

  }

  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }

    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return value;
  }

  onInputChange(event: any) {
    this.sliderValue = Number(event.value);
    this.numbers = [Number(event.value)];
  }
}
