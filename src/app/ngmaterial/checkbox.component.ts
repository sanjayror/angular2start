import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-checkbox',
    template: `
                <div *ngFor="let Item of data">
                     <input type="checkbox" />{{Item.name}}
                </div>
                `,
    styleUrls: ['./ngmaterial.component.css']
})
export class CheckboxComponent implements OnInit {
    @Input() sliderValue: number;
    data: any[] = [];

    constructor() { }

    ngOnInit() {
        this.data = [];
        for (let index = 0; index < this.sliderValue; index++) {
            this.data.push({ 'id': index + 1, 'name': 'Name:' + (index + 1) });
        }
        console.log('This is emitted as the thumb slides value : ' + this.data);
    }
}
